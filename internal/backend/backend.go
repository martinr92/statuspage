/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */
package backend

import "time"

type Backend struct {
	jobTick  time.Ticker
	jobQueue chan Job
}

var Instance = newBackend()

func newBackend() *Backend {
	// initialize new backend instance
	backend := &Backend{
		jobTick: *time.NewTicker(time.Minute),
	}

	// return new backend
	return backend
}

func (backend *Backend) Start() {
	go backend.startJobs()
}
