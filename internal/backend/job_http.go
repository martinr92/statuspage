/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */
package backend

import (
	"fmt"
	"net/http"

	"gitlab.com/martinr92/statuspage/internal/database"
)

type JobProcessorHTTP struct {
}

func (jobHTTP *JobProcessorHTTP) Check(monitorJob database.MonitorJob) (response JobProcessorCheckResponse) {
	// prepare HTTP client
	httpClient := http.Client{}

	// check for redirect configuration
	if !monitorJob.FollowRedirect {
		httpClient.CheckRedirect = func(req *http.Request, via []*http.Request) error {
			return http.ErrUseLastResponse
		}
	}

	// send HTTP call
	httpResponse, err := httpClient.Get(monitorJob.URL)
	if err != nil {
		response.Message = fmt.Sprintf("http call failed: %s", err)
		return response
	}

	// check response status code
	if monitorJob.CheckStatusCode {
		if httpResponse.StatusCode != monitorJob.StatusCode {
			response.Message = fmt.Sprintf("received status code %d instead of %d", httpResponse.StatusCode, monitorJob.StatusCode)
			return response
		}
	}

	// no error reported until now
	response.Success = true
	return response
}
