/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */
package backend

import (
	"log"

	"gitlab.com/martinr92/statuspage/internal/database"
)

const parallelCalls = 10

type Job struct {
	MonitorJob database.MonitorJob
	Processor  JobProcessorInterface
}

type JobProcessorInterface interface {
	Check(database.MonitorJob) JobProcessorCheckResponse
}

type JobProcessorCheckResponse struct {
	Success bool
	Message string
}

func (backend *Backend) startJobs() {
	// start background processors
	for i := 0; i < parallelCalls; i++ {
		go backend.jobProcessor()
	}

	for {
		// wait for next job ticker (1 minute)
		<-backend.jobTick.C

		// search for relevant jobs
		monitorJobs := database.Instance.GetActiveMonitorJobs()

		// enqueue each job
		for _, monitorJob := range monitorJobs {
			newJob := Job{
				MonitorJob: monitorJob,
			}
			backend.jobQueue <- newJob
		}
	}
}

func (backend *Backend) jobProcessor() {
	for {
		// wait for next job
		job := <-backend.jobQueue

		// find job processor
		switch job.MonitorJob.Type {
		case database.MonitorJobTypeHTTP:
			job.Processor = &JobProcessorHTTP{}
		default:
			log.Fatalln("invalid / unknown monitor job type", job.MonitorJob.Type)
		}

		// execute check
		jobResponse := job.Processor.Check(job.MonitorJob)

		// update job (last checked)
		if jobResponse.Success {
			database.Instance.UpdateMonitorJob(job.MonitorJob.ID, database.MonitorJobStatusSuccess, jobResponse.Message, 0)
		} else {
			errorCounter := job.MonitorJob.ErrorCounter + 1
			newStatus := database.MonitorJobStatusPendingError
			if errorCounter >= 3 {
				newStatus = database.MonitorJobStatusError
			}
			database.Instance.UpdateMonitorJob(job.MonitorJob.ID, newStatus, jobResponse.Message, errorCounter)
		}
	}
}
