/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */
package backend

import (
	"testing"

	"gitlab.com/martinr92/statuspage/internal/database"
)

func TestHTTPJob(t *testing.T) {
	httpJob := JobProcessorHTTP{}
	result := httpJob.Check(database.MonitorJob{
		URL:             "https://www.martin-riedl.de",
		StatusCode:      200,
		CheckStatusCode: true,
		FollowRedirect:  false,
	})

	if !result.Success {
		t.Error("http job check failed")
	}
}

func TestHTTPJobInvalidResponse(t *testing.T) {
	httpJob := JobProcessorHTTP{}
	result := httpJob.Check(database.MonitorJob{
		URL:             "http://www.martin-riedl.de",
		StatusCode:      200,
		CheckStatusCode: true,
		FollowRedirect:  false,
	})

	if result.Success {
		t.Error("expected error for invalid response status code")
	}
}

func TestHTTPJobInvalidHost(t *testing.T) {
	httpJob := JobProcessorHTTP{}
	result := httpJob.Check(database.MonitorJob{
		URL:            "http://not-exists.martin-riedl.de",
		StatusCode:     200,
		FollowRedirect: false,
	})

	if result.Success {
		t.Error("expected error for invalid host")
	}
}
