/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */
package database

import "testing"

func TestDashboard(t *testing.T) {
	// create new dashboard
	dashboard := &Dashboard{
		Name:   "Testing",
		Slug:   "testing",
		Domain: "testing.awesomedomain.yeaa",
	}
	if err := Instance.CreateDashboard(dashboard); err != nil {
		t.Error("unable to create dashboard", err)
	}

	// validate ID
	if dashboard.ID == 0 {
		t.Error("received invalid ID from dashboard")
	}

	// load dashboard
	db2 := Instance.GetDashboard(dashboard.ID)
	if db2.ID != dashboard.ID || db2.Name != dashboard.Name {
		t.Error("received invalid data from GetDashboard")
	}

	// load all dashboards
	dashboards := Instance.GetDashboards()
	if len(dashboards) == 0 {
		t.Error("missing dashboard in GetDashboards")
	}

	// find dashboad by slug
	dbSlug := Instance.FindDashboardBySlug(dashboard.Slug)
	if dbSlug == nil {
		t.Error("unable to find dashboard by slug")
	}

	// find dashboard by domain
	dbDomain := Instance.FindDashboardByDomain(dashboard.Domain)
	if dbDomain == nil {
		t.Error("unable to find dashboard by domain")
	}

	// create new dashboard with the same slug (slug is unique)
	err := Instance.CreateDashboard(&Dashboard{
		Slug: dashboard.Slug,
	})
	if err == nil {
		t.Error("expected unique constraint error (for slug of dashboard)")
	}
}

func TestDashboadInvalidID(t *testing.T) {
	dashboard := Instance.GetDashboard(999999)
	if dashboard != nil {
		t.Error("received invalid dashboard (by non-existing ID)")
	}
}

func TestDashboardSlug(t *testing.T) {
	dashboard := Dashboard{
		Slug: "invalid!character",
	}
	if dashboard.ValidateSlug() {
		t.Error("slug validation of dashboard failed")
	}
}

func TestDashboadUnknownSlug(t *testing.T) {
	dashboard := Instance.FindDashboardBySlug("not-exists")
	if dashboard != nil {
		t.Error("received invalid dashboard by (non existing) slug")
	}
}

func TestDashboardUnknownDomain(t *testing.T) {
	dashboard := Instance.FindDashboardByDomain("not-existing.domain")
	if dashboard != nil {
		t.Error("received invalid dashboard by (non existing) domain")
	}
}
