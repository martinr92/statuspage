/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */
package database

import (
	"regexp"
	"time"
)

type Dashboard struct {
	ID        uint `gorm:"primaryKey"`
	Name      string
	Slug      string      `gorm:"unique"`
	Domain    string      `gorm:"unique"`
	Incidents []*Incident `gorm:"many2many:incident_dashboards"`
	CreatedAt time.Time
	UpdatedAt time.Time
}

func (dashboard *Dashboard) ValidateSlug() bool {
	re := regexp.MustCompile(`^[a-z0-9_-]+$`)
	return re.MatchString(dashboard.Slug)
}

func (db *DB) GetDashboards() []Dashboard {
	var dashboard []Dashboard
	db.db.Find(&dashboard)
	return dashboard
}

func (db *DB) GetDashboardsByIDs(ids []uint) []Dashboard {
	var dashboards []Dashboard
	db.db.Find(&dashboards, ids)
	return dashboards
}

func (db *DB) GetDashboard(id uint) *Dashboard {
	var dashboard Dashboard
	result := db.db.Find(&dashboard, id)
	if result.RowsAffected == 0 {
		return nil
	}

	return &dashboard
}

func (db *DB) FindDashboardBySlug(slug string) *Dashboard {
	var dashboard Dashboard
	result := db.db.Where(&Dashboard{
		Slug: slug,
	}).Find(&dashboard)
	if result.RowsAffected == 0 {
		return nil
	}

	return &dashboard
}

func (db *DB) FindDashboardByDomain(domain string) *Dashboard {
	var dashboard Dashboard
	result := db.db.Where(&Dashboard{
		Domain: domain,
	}).Find(&dashboard)
	if result.RowsAffected == 0 {
		return nil
	}

	return &dashboard
}

func (db *DB) CreateDashboard(dashboard *Dashboard) error {
	result := db.db.Create(dashboard)
	if result.Error != nil {
		return result.Error
	}
	return nil
}
