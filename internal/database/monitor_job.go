/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */
package database

import "time"

type MonitorJob struct {
	ID              uint `gorm:"primaryKey"`
	MonitorID       uint
	Monitor         Monitor
	Type            MonitorJobType
	URL             string
	StatusCode      int
	CheckStatusCode bool
	FollowRedirect  bool
	Enabled         bool
	Status          MonitorJobStatus
	ErrorCounter    int
	LastMessage     string
	LastChecked     time.Time
	CreatedAt       time.Time
	UpdatedAt       time.Time
}

type MonitorJobType int

const (
	MonitorJobTypeHTTP MonitorJobType = 1
)

type MonitorJobStatus int

const (
	MonitorJobStatusInitial        MonitorJobStatus = 1
	MonitorJobStatusSuccess        MonitorJobStatus = 2
	MonitorJobStatusPendingSuccess MonitorJobStatus = 3
	MonitorJobStatusPendingError   MonitorJobStatus = 4
	MonitorJobStatusError          MonitorJobStatus = 5
)

func (db *DB) GetActiveMonitorJobs() []MonitorJob {
	var jobs []MonitorJob
	db.db.Where(&MonitorJob{
		Enabled: true,
	}).Find(&jobs)
	return jobs
}

func (db *DB) UpdateMonitorJob(monitorJobID uint, status MonitorJobStatus, lastMessage string, newErrorCounter int) {
	db.db.Model(&MonitorJob{}).Select("Status", "LastMessage", "ErrorCounter", "LastChecked").Updates(MonitorJob{
		ID:           monitorJobID,
		Status:       status,
		LastMessage:  lastMessage,
		ErrorCounter: newErrorCounter,
		LastChecked:  time.Now(),
	})
}
