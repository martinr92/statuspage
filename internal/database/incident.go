/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */
package database

import (
	"database/sql"
	"time"
)

type Incident struct {
	ID          uint `gorm:"primaryKey"`
	Title       string
	Description string
	Severity    IncidentSeverity
	StartAt     time.Time
	EndAt       sql.NullTime
	IsScheduled bool
	Status      IncidentStatus
	Dashboards  []*Dashboard `gorm:"many2many:incident_dashboards"`
	CreatedAt   time.Time
	UpdatedAt   time.Time
}

type IncidentSeverity int

const (
	IncidentSeverityInfo    IncidentSeverity = 1
	IncidentSeverityWarning IncidentSeverity = 2
	IncidentSeverityError   IncidentSeverity = 3
)

type IncidentStatus int

const (
	IncidentStatusPlanned IncidentStatus = 1
	IncidentStatusOpen    IncidentStatus = 2
	IncidentStatusClosed  IncidentStatus = 3
)

func (db *DB) CreateIncident(incident *Incident) error {
	result := db.db.Omit("Dashboards.*").Create(incident)
	if result.Error != nil {
		return result.Error
	}
	return nil
}

func (db *DB) GetIncidents() []Incident {
	var incidents []Incident
	db.db.Find(&incidents)
	return incidents
}

func (db *DB) FindIncidentsByDashboard(dashboardID uint) ([]Incident, error) {
	var incidents []Incident
	err := db.db.Model(&Dashboard{
		ID: dashboardID,
	}).Association("Incidents").Find(&incidents)
	return incidents, err
}
