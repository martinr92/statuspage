/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */
package database

import (
	"testing"
	"time"
)

func TestIncident(t *testing.T) {
	// create new incident
	incident := &Incident{
		Title:       "Incident A",
		Description: "Something is wrong",
		Severity:    IncidentSeverityWarning,
		IsScheduled: false,
		StartAt:     time.Now(),
		Status:      IncidentStatusOpen,
	}
	if err := Instance.CreateIncident(incident); err != nil {
		t.Error("unable to create new incident", err)
	}

	// validate ID
	if incident.ID == 0 {
		t.Error("received invalid ID for new incident")
	}

	// load all incidents
	incidents := Instance.GetIncidents()
	if len(incidents) == 0 {
		t.Error("received no incidents from GetIncidents")
	}
}
