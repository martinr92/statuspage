/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */
package database

import (
	"log"

	"github.com/glebarez/sqlite"
	"gorm.io/gorm"
)

type DB struct {
	db *gorm.DB
}

var Instance = newDB()

func newDB() *DB {
	return &DB{}
}

func (db *DB) OpenSQLite(file string) error {
	// open connection
	gormdb, err := gorm.Open(sqlite.Open(file))
	if err != nil {
		log.Println(err)
		return err
	}
	db.db = gormdb

	// upgrade schema
	if err := db.db.AutoMigrate(&Dashboard{}, &Monitor{}, &MonitorJob{}, &Incident{}, &IncidentComment{}); err != nil {
		log.Println(err)
		return err
	}

	return nil
}
