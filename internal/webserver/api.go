/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */
package webserver

import (
	"encoding/json"
	"encoding/xml"
	"io"
	"log"
	"net/http"
	"reflect"
	"strings"
)

const (
	ContentTypeApplicationJSON = "application/json"
	ContentTypeApplicationXML  = "application/xml"
)

type ResponseStatus interface {
	StatusNumber() int
}

type GenericResponse struct {
	StatusCode int    `json:"status" xml:"statusCode"`
	Message    string `json:"message" xml:"message"`
}

func (genericResponse GenericResponse) StatusNumber() int {
	return genericResponse.StatusCode
}

type Error GenericResponse

func (err Error) StatusNumber() int {
	return err.StatusCode
}

var badRequestError = Error{
	StatusCode: http.StatusBadRequest,
	Message:    "bad request",
}

var notFoundError = Error{
	StatusCode: http.StatusNotFound,
	Message:    "not found",
}

var internalServerError = Error{
	StatusCode: http.StatusInternalServerError,
	Message:    "internal server error",
}

type responseWrapper struct {
	XMLName xml.Name `json:"-" xml:"response"`
	Content any
}

func (webServer *WebServer) sendAPIResponse(w http.ResponseWriter, r *http.Request, content any) {
	// check for nil response
	if reflect.TypeOf(content).Kind() == reflect.Pointer && reflect.ValueOf(content).IsNil() {
		webServer.sendAPIResponse(w, r, notFoundError)
		return
	}

	// get requested response format
	acceptHeader := strings.ToLower(r.Header.Get("accept"))

	// send response in requested format
	var data []byte
	switch acceptHeader {
	case "*/*":
		acceptHeader = ContentTypeApplicationJSON
		fallthrough
	case ContentTypeApplicationJSON:
		jsonData, err := json.Marshal(content)
		if err != nil {
			log.Println("unable to marshal JSON response", err)
			webServer.sendAPIResponse(w, r, internalServerError)
			return
		}
		data = jsonData
	case ContentTypeApplicationXML:
		// arrays needs a wrapper
		xmlConent := content
		if reflect.TypeOf(xmlConent).Kind() == reflect.Slice {
			xmlConent = responseWrapper{
				Content: xmlConent,
			}
		}

		// marshal XML
		xmlData, err := xml.Marshal(xmlConent)
		if err != nil {
			log.Println("unable to marshal XML response", err)
			webServer.sendAPIResponse(w, r, internalServerError)
			return
		}
		data = []byte(xml.Header)
		data = append(data, xmlData...)
	default:
		r.Header.Set("accept", ContentTypeApplicationJSON)
		webServer.sendAPIResponse(w, r, Error{
			StatusCode: http.StatusUnsupportedMediaType,
			Message:    "invalid accept header",
		})
		return
	}

	// set additional header
	w.Header().Add("Content-Type", acceptHeader)
	w.Header().Add("Cache-Control", "no-cache")

	// set status code
	if responseStatus, ok := content.(ResponseStatus); ok {
		w.WriteHeader(responseStatus.StatusNumber())
	}

	// send response
	if _, err := w.Write(data); err != nil {
		log.Println("unable to send response", err)
	}
}

func parseRequestBody[T any](webServer *WebServer, w http.ResponseWriter, r *http.Request) *T {
	// read body
	bodyData, err := io.ReadAll(r.Body)
	if err != nil {
		log.Println("reading request body failed", err)
		webServer.sendAPIResponse(w, r, internalServerError)
		return nil
	}

	// get request body format
	contentType := r.Header.Get("content-type")

	// parse request
	var obj T
	switch contentType {
	case ContentTypeApplicationJSON:
		if err := json.Unmarshal(bodyData, &obj); err != nil {
			log.Println("unable to unmarshal request body JSON", err)
			webServer.sendAPIResponse(w, r, badRequestError)
			return nil
		}
	case ContentTypeApplicationXML:
		if err := xml.Unmarshal(bodyData, &obj); err != nil {
			log.Println("unable to unmarshal request body XML", err)
			webServer.sendAPIResponse(w, r, badRequestError)
			return nil
		}
	default:
		webServer.sendAPIResponse(w, r, badRequestError)
		return nil
	}

	return &obj
}
