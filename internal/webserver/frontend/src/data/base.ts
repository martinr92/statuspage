/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */
const fetchCall = async function (method: string, config, data: any = null) {
  return new Promise(function (resolve, reject) {
    let fetchDetails: RequestInit = {
      method: method,
      headers: {
        "content-type": "application/json"
      }
    };
    if (data) {
      fetchDetails.body = JSON.stringify(data);
    }
    fetch(config.url, fetchDetails).then(response => {
      // check for http error
      if (!response.ok) {
        response.json()
          .catch(() => {
            reject({
              "message": "received invalid data from server"
            })
          })
          .then(json => {
            reject(json);
          });
        return;
      }

      // everything ok, check for redirection
      if (response.status == 201) {
        get({
          url: response.headers.get("location")
        }).then(redirectResponse => {
          resolve(redirectResponse);
        });
        return;
      } else if (response.status == 204) {
        // everything ok, check for "no content"
        resolve({});
        return;
      }

      // parse response
      response.json().then(json => {
        resolve(json);
      });
    })
  });
};

export async function get(config) {
  return fetchCall("GET", config);
};

export async function post(config, data) {
  return fetchCall("POST", config, data);
};

export async function patch(config, data) {
  return fetchCall("PATCH", config, data);
};

export async function del(config, data) {
  return fetchCall("delete", config, data);
};
