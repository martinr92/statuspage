/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */
package webserver

import (
	"context"
	"log"
	"net/http"
	"time"

	httprouter "gitlab.com/martinr92/gohttprouter/v2"
	"golang.org/x/sync/errgroup"
)

type WebServer struct {
	Router *httprouter.Router
	http   *http.Server
	cancel context.CancelFunc
}

var Instance = newWebServer()

func newWebServer() *WebServer {
	webServer := &WebServer{
		Router: httprouter.New(),
	}

	// register routes
	webServer.Router.HandleFunc(http.MethodGet, "/api/v1/dashboards", webServer.serveAPIDashbaord)
	webServer.Router.HandleFunc(http.MethodGet, "/api/v1/dashboards/:slug", webServer.serveAPIDashbaord)
	webServer.Router.Handle(http.MethodGet, "/api/v1/admin/dashboards", adminHandler{Func: webServer.serveAPIAdminDashbaords})
	webServer.Router.Handle(http.MethodPost, "/api/v1/admin/dashboards", adminHandler{Func: webServer.serveAPIAdminDashbaordCreate})
	webServer.Router.Handle(http.MethodGet, "/api/v1/admin/dashboards/:id", adminHandler{Func: webServer.serveAPIAdminDashbaord})
	webServer.Router.Handle(http.MethodGet, "/api/v1/admin/incidents", adminHandler{Func: webServer.serveAPIAdminIncidents})
	webServer.Router.Handle(http.MethodPost, "/api/v1/admin/incidents", adminHandler{Func: webServer.serveAPIAdminIncidentCreate})

	return webServer
}

func (webServer *WebServer) Run(listen string) error {
	// create new web server instance
	webServer.http = &http.Server{
		Addr:    listen,
		Handler: webServer,
	}

	// start background handling
	ctx, cancel := context.WithCancel(context.Background())
	webServer.cancel = cancel
	g, gCtx := errgroup.WithContext(ctx)

	// start server
	g.Go(func() error {
		defer webServer.cancel()
		log.Println("starting webserver on", webServer.http.Addr)
		err := webServer.http.ListenAndServe()
		if err != http.ErrServerClosed {
			return err
		}
		return nil
	})
	// register shutdown
	g.Go(func() error {
		// wait for closed context
		<-gCtx.Done()

		// create new context for HTTP timeout handling (wait 10 seconds; then close anyway)
		timeoutContext, cancel := context.WithTimeout(context.Background(), 10*time.Second)
		defer cancel()
		return webServer.http.Shutdown(timeoutContext)
	})

	// wait until webserver closes
	if err := g.Wait(); err != nil {
		log.Println(err)
		return err
	}
	return nil
}

func (webServer *WebServer) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// TODO: check domain of dashboards

	webServer.Router.ServeHTTP(w, r)
}

func (webServer *WebServer) Shutdown() {
	// send cancel request
	webServer.cancel()
}
