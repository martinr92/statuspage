/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */
package webserver

import (
	"fmt"
	"log"
	"net/http"
	"strconv"

	httprouter "gitlab.com/martinr92/gohttprouter/v2"
	"gitlab.com/martinr92/statuspage/internal/database"
)

type adminDashboardRequest struct {
	Name   string
	Slug   string
	Domain string
}

type adminDashboardResponse struct {
	ID     uint   `json:"id"`
	Name   string `json:"name"`
	Slug   string `json:"slug"`
	Domain string `json:"domain"`
}

func newAdminDahsboardResponse(dashboard database.Dashboard) adminDashboardResponse {
	return adminDashboardResponse{
		ID:     dashboard.ID,
		Name:   dashboard.Name,
		Slug:   dashboard.Slug,
		Domain: dashboard.Domain,
	}
}

func (webServer *WebServer) serveAPIAdminDashbaordCreate(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) {
	// parse request
	dashboardRequest := parseRequestBody[adminDashboardRequest](webServer, w, r)
	if dashboardRequest == nil {
		return
	}

	// TODO: check, if domain or slug is already used

	// build and validate dashboard entry
	dashboard := &database.Dashboard{
		Name:   dashboardRequest.Name,
		Slug:   dashboardRequest.Slug,
		Domain: dashboardRequest.Domain,
	}
	if !webServer.validateDashboard(w, r, dashboard) {
		return
	}

	// create dashboard
	err := database.Instance.CreateDashboard(dashboard)
	if err != nil {
		log.Println("unable to create new dashboard", err)
		webServer.sendAPIResponse(w, r, internalServerError)
		return
	}

	// redirect to new ID
	w.Header().Add("location", fmt.Sprintf("/api/v1/admin/dashboards/%d", dashboard.ID))
	w.WriteHeader(http.StatusCreated)
}

func (webServer *WebServer) validateDashboard(w http.ResponseWriter, r *http.Request, dashboard *database.Dashboard) bool {
	// validate name
	if dashboard.Name == "" {
		log.Println("dashboard name is empty")
		webServer.sendAPIResponse(w, r, Error{
			StatusCode: http.StatusBadRequest,
			Message:    "name is empty",
		})
		return false
	}

	// validate slug
	if !dashboard.ValidateSlug() {
		log.Println("dashboard slug is invalid")
		webServer.sendAPIResponse(w, r, Error{
			StatusCode: http.StatusBadRequest,
			Message:    "invalid slug",
		})
		return false
	}

	return true
}

func (webServer *WebServer) serveAPIAdminDashbaords(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) {
	// load dashboards
	dashboards := database.Instance.GetDashboards()

	// send dashboard
	response := []adminDashboardResponse{}
	for _, dashboard := range dashboards {
		response = append(response, newAdminDahsboardResponse(dashboard))
	}
	webServer.sendAPIResponse(w, r, response)
}

func (webServer *WebServer) serveAPIAdminDashbaord(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) {
	// parse dashboard ID
	idString := info.Parameters["id"]
	id, err := strconv.Atoi(idString)
	if err != nil {
		webServer.sendAPIResponse(w, r, badRequestError)
		return
	}

	// load dashboard
	dashboard := database.Instance.GetDashboard(uint(id))
	if dashboard == nil {
		webServer.sendAPIResponse(w, r, notFoundError)
		return
	}

	// send dashboard
	response := newAdminDahsboardResponse(*dashboard)
	webServer.sendAPIResponse(w, r, response)
}
