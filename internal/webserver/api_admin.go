/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */
package webserver

import (
	"net/http"

	httprouter "gitlab.com/martinr92/gohttprouter/v2"
)

type adminHandler struct {
	Func httprouter.RouteHandler
}

func (handler adminHandler) ServeHTTP(w http.ResponseWriter, r *http.Request, routingInfo httprouter.RoutingInfo) {
	// TODO: check autentication
	handler.Func(w, r, routingInfo)
}
