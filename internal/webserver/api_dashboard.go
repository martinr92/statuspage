/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */
package webserver

import (
	"log"
	"net/http"
	"time"

	httprouter "gitlab.com/martinr92/gohttprouter/v2"
	"gitlab.com/martinr92/statuspage/internal/database"
)

type dashbaordResponse struct {
	Name      string             `json:"name"`
	Incidents []incidentResponse `json:"incidents"`
}

type incidentResponse struct {
	ID          uint       `json:"id"`
	Title       string     `json:"title"`
	Description string     `json:"description"`
	Severity    string     `json:"severity"`
	StartAt     time.Time  `json:"start"`
	EndAt       *time.Time `json:"end"`
	Status      string     `json:"status"`
}

func (webServer *WebServer) serveAPIDashbaord(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) {
	// check for slug
	slug := info.Parameters["slug"]
	domain := r.URL.Query().Get("domain")

	// find dashboard by slug or domain
	var dashboard database.Dashboard
	if slug != "" {
		db := database.Instance.FindDashboardBySlug(slug)
		if db == nil {
			webServer.sendAPIResponse(w, r, notFoundError)
			return
		}
		dashboard = *db
	} else if domain != "" {
		db := database.Instance.FindDashboardByDomain(domain)
		if db == nil {
			webServer.sendAPIResponse(w, r, notFoundError)
			return
		}
		dashboard = *db
	} else {
		webServer.sendAPIResponse(w, r, badRequestError)
		return
	}

	// load last 7 days incidents
	incidents, success := webServer.findLatestIncidentsForDashboard(w, r, dashboard.ID)
	if !success {
		return
	}

	// serve response
	response := dashbaordResponse{
		Name:      dashboard.Name,
		Incidents: incidents,
	}
	webServer.sendAPIResponse(w, r, response)
}

func (webServer *WebServer) findLatestIncidentsForDashboard(w http.ResponseWriter, r *http.Request, dashboardID uint) ([]incidentResponse, bool) {
	// load incidents
	incidents, err := database.Instance.FindIncidentsByDashboard(dashboardID)
	if err != nil {
		log.Println(err)
		webServer.sendAPIResponse(w, r, internalServerError)
		return nil, false
	}

	// filter and map to response struct
	response := []incidentResponse{}
	for _, incident := range incidents {
		// show only active, planned and history of last 7 days
		if incident.Status == database.IncidentStatusOpen || incident.Status == database.IncidentStatusPlanned || incident.StartAt.Add(time.Hour*24*7).Before(time.Now()) {
			response = append(response, newIncidentResponse(incident))
		}
	}

	return response, true
}

func newIncidentResponse(incident database.Incident) incidentResponse {
	response := incidentResponse{
		ID:          incident.ID,
		Title:       incident.Title,
		Description: incident.Description,
		Severity:    IncidentSeverity[incident.Severity],
		StartAt:     incident.StartAt,
		Status:      IncidentStatus[incident.Status],
	}

	if incident.EndAt.Valid {
		response.EndAt = &incident.EndAt.Time
	}

	return response
}
