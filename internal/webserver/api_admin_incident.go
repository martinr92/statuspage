/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */
package webserver

import (
	"database/sql"
	"fmt"
	"log"
	"net/http"
	"time"

	httprouter "gitlab.com/martinr92/gohttprouter/v2"
	"gitlab.com/martinr92/statuspage/internal/database"
)

type adminIncidentRequest struct {
	Title       string     `json:"title"`
	Description string     `json:"description"`
	Severity    string     `json:"severity"`
	StartAt     time.Time  `json:"start"`
	EndAt       *time.Time `json:"end"`
	IsScheduled bool       `json:"isScheduled"`
	Status      string     `json:"status"`
	Dashboards  []uint     `json:"dashboards"`
}

type adminIncidentResponse struct {
	ID          uint       `json:"id"`
	Title       string     `json:"title"`
	Description string     `json:"description"`
	Severity    string     `json:"severity"`
	StartAt     time.Time  `json:"start"`
	EndAt       *time.Time `json:"end"`
	IsScheduled bool       `json:"isScheduled"`
	Status      string     `json:"status"`
}

var (
	IncidentSeverity = map[database.IncidentSeverity]string{
		1: "info",
		2: "warning",
		3: "error",
	}
	IncidentSeverityReverse = reverseMap[database.IncidentSeverity, string](IncidentSeverity)

	IncidentStatus = map[database.IncidentStatus]string{
		1: "planned",
		2: "open",
		3: "closed",
	}
	IncidentStatusReverse = reverseMap[database.IncidentStatus, string](IncidentStatus)
)

func newAdminIncidentResponse(incident database.Incident) adminIncidentResponse {
	response := adminIncidentResponse{
		ID:          incident.ID,
		Title:       incident.Title,
		Description: incident.Description,
		Severity:    IncidentSeverity[incident.Severity],
		StartAt:     incident.StartAt,
		IsScheduled: incident.IsScheduled,
		Status:      IncidentStatus[incident.Status],
	}

	if incident.EndAt.Valid {
		response.EndAt = &incident.EndAt.Time
	}

	return response
}

func (webServer *WebServer) serveAPIAdminIncidents(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) {
	// load incidents
	incidents := database.Instance.GetIncidents()

	// send incidents
	response := []adminIncidentResponse{}
	for _, incident := range incidents {
		response = append(response, newAdminIncidentResponse(incident))
	}
	webServer.sendAPIResponse(w, r, response)
}

func (webServer *WebServer) serveAPIAdminIncidentCreate(w http.ResponseWriter, r *http.Request, info httprouter.RoutingInfo) {
	// parse request
	incidentRequest := parseRequestBody[adminIncidentRequest](webServer, w, r)
	if incidentRequest == nil {
		return
	}

	// build and validate incident entry
	incident := &database.Incident{
		Title:       incidentRequest.Title,
		Description: incidentRequest.Description,
		Severity:    IncidentSeverityReverse[incidentRequest.Severity],
		StartAt:     incidentRequest.StartAt,
		IsScheduled: incidentRequest.IsScheduled,
		Status:      IncidentStatusReverse[incidentRequest.Status],
	}
	if incidentRequest.EndAt != nil {
		incident.EndAt = sql.NullTime{Valid: true, Time: *incidentRequest.EndAt}
	}
	if incidentRequest.Dashboards != nil {
		incident.Dashboards = []*database.Dashboard{}
		for _, dashboardID := range incidentRequest.Dashboards {
			incident.Dashboards = append(incident.Dashboards, &database.Dashboard{ID: dashboardID})
		}
	}
	if !webServer.validateIncident(w, r, incident) {
		return
	}

	// create incident
	err := database.Instance.CreateIncident(incident)
	if err != nil {
		log.Println("unable to create new incident", err)
		webServer.sendAPIResponse(w, r, internalServerError)
		return
	}

	// redirect to new ID
	w.Header().Add("location", fmt.Sprintf("/api/v1/admin/incidents/%d", incident.ID))
	w.WriteHeader(http.StatusCreated)
}

func (webServer *WebServer) validateIncident(w http.ResponseWriter, r *http.Request, incident *database.Incident) bool {
	// validate start/end date
	if incident.EndAt.Valid && incident.StartAt.After(incident.EndAt.Time) {
		log.Println("invalid start/end date of incident", incident.StartAt, incident.EndAt)
		webServer.sendAPIResponse(w, r, Error{
			StatusCode: http.StatusBadRequest,
			Message:    "invalid start/end date",
		})
		return false
	}

	// validate severity
	if _, found := IncidentSeverity[incident.Severity]; !found {
		log.Println("invalid incident severity", incident.Severity)
		webServer.sendAPIResponse(w, r, Error{
			StatusCode: http.StatusBadRequest,
			Message:    "invalid severity",
		})
		return false
	}

	// validate status
	if _, found := IncidentStatus[incident.Status]; !found {
		log.Println("invalid incident status", incident.Status)
		webServer.sendAPIResponse(w, r, Error{
			StatusCode: http.StatusBadRequest,
			Message:    "invalid status",
		})
		return false
	}

	// validate dashboard IDs
	if incident.Dashboards != nil {
		dashboardIDs := []uint{}
		for _, dashboard := range incident.Dashboards {
			dashboardIDs = append(dashboardIDs, dashboard.ID)
		}

		// load dashboards
		dashboards := database.Instance.GetDashboardsByIDs(dashboardIDs)
		if len(dashboardIDs) != len(dashboards) {
			log.Println("invalid dashboard ID for incident", dashboardIDs)
			webServer.sendAPIResponse(w, r, Error{
				StatusCode: http.StatusBadRequest,
				Message:    "invalid dashboard ID",
			})
			return false
		}
	}

	return true
}
