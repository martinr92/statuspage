/** @type {import('tailwindcss').Config} */
export default {
  content: ['./internal/webserver/frontend/**/*.{vue,js,html}'],
  theme: {
    extend: {},
  },
  plugins: [],
}
