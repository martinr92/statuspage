/*
 * Copyright (C) 2023 Martin Riedl
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the Server Side Public License, version 1,
 * as published by MongoDB, Inc.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * Server Side Public License for more details.
 *
 * You should have received a copy of the Server Side Public License
 * along with this program. If not, see
 * <http://www.mongodb.com/licensing/server-side-public-license>.
 */
package main

import (
	"context"
	"flag"
	"fmt"
	"log"
	"os"
	"os/signal"
	"sync"
	"syscall"

	"gitlab.com/martinr92/statuspage/internal/backend"
	"gitlab.com/martinr92/statuspage/internal/database"
	"gitlab.com/martinr92/statuspage/internal/version"
	"gitlab.com/martinr92/statuspage/internal/webserver"
)

var flagSetServer = flag.NewFlagSet("server", flag.ContinueOnError)
var flagServerDBSQLiteLocation = flagSetServer.String("dbSQLiteLocation", "database.db", "location of SQLite database file")
var flagServerWebserverListen = flagSetServer.String("webserverListen", ":8080", "listening address for webserver")

func main() {
	// check for flag set
	if len(os.Args) <= 1 {
		showHelp()
		os.Exit(1)
	}

	// flag handling based on flag set
	switch os.Args[1] {
	case "server":
		if err := flagSetServer.Parse(os.Args[2:]); err != nil {
			os.Exit(1)
		}

		// start server instance
		runServer()
	case "version":
		fmt.Println(version.Version)
	case "help":
		showHelp()
	default:
		fmt.Println("invalid subcommand", os.Args[1])
		fmt.Println()
		showHelp()
		os.Exit(1)
	}
}

func showHelp() {
	fmt.Println("server: run as main server process")
	flagSetServer.PrintDefaults()
	fmt.Println()
	fmt.Println("version: shows the binary version")
	fmt.Println()
	fmt.Println("help: shows this help")
	fmt.Println()
}

func runServer() {
	// start backend
	initDB()
	backend.Instance.Start()

	// wait for shutdown request
	signalChannel := make(chan os.Signal, 1)

	// Ctrl+C ==> Interrupt
	signal.Notify(signalChannel, os.Interrupt, syscall.SIGTERM)

	// create new shutdown context for webserver
	webServerContext, cancel := context.WithCancel(context.Background())

	// wait for signal
	go func() {
		<-signalChannel
		log.Println("shutdown signal received")
		cancel()
	}()

	// run webserver
	runWebServer(webServerContext, cancel)
}

func initDB() {
	dbLocation := getEnvString("DB_SQLITE_LOCATION", *flagServerDBSQLiteLocation)
	database.Instance.OpenSQLite(dbLocation)
}

func runWebServer(ctx context.Context, cancel context.CancelFunc) {
	// retrieve config
	webServerListen := getEnvString("WEBSERVER_LISTEN", *flagServerWebserverListen)

	var waitGroup sync.WaitGroup
	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()

		// start and run webserver
		if err := webserver.Instance.Run(webServerListen); err != nil {
			log.Fatal("unable to start webserver")
			cancel()
			return
		}
	}()
	waitGroup.Add(1)
	go func() {
		defer waitGroup.Done()
		<-ctx.Done()
		log.Println("initiate webserver shutdown")
		webserver.Instance.Shutdown()
	}()

	waitGroup.Wait()
	log.Println("webserver closed")
}

func getEnvString(name string, defaultValue string) string {
	val := os.Getenv(name)
	if val != "" {
		return val
	}
	return defaultValue
}
